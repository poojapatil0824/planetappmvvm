//
//  PlanetListTableViewCell.swift
//  PlanetAppMVVM
//
//  Created by Pooja Awati on 18/08/2019.
//  Copyright © 2019 Pooja Awati. All rights reserved.
//

import Foundation
import UIKit

class PlanetListTableViewCell: UITableViewCell {
    @IBOutlet weak var planetNameLabel: UILabel!

}
