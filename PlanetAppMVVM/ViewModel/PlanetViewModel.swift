//
//  PlanetViewModel.swift
//  PlanetAppMVVM
//
//  Created by Pooja Awati on 18/08/2019.
//  Copyright © 2019 Pooja Awati. All rights reserved.
//

import Foundation

class PlanetViewModel {

    private let planetWebService:PlanetWebService

    init(planetWebService: PlanetWebService){
        self.planetWebService = planetWebService
    }

    func fetchPlanetDetails() -> [PlanetDetails]? {
        return PlanetDataStore.retrievePlanetDetails()
    }


    public func getPlanetResults() {
        getPlanetDetails() { (planet, error) in
            if let error = error {
                print("Error : \(error.localizedDescription)")
                return
            }
            guard let planet = planet  else { return }
            //write planet results to file
            PlanetDataStore.store(planetDetails: planet.results)
            NotificationCenter.default.post(name: NSNotification.Name.UpdatePlanetDetails, object: nil)
        }
    }


    private func getPlanetDetails(completion: @escaping (_ planet : Planet?, _ error: Error?) -> Void) {
        planetWebService.fetchPlanetData() { (planet, error) in
            if let error = error {
                print("Error : \(error.localizedDescription)")
                return
            }
            guard let planet = planet  else { return }
            return completion(planet, nil)
        }

    }
}

extension NSNotification.Name {
    static var UpdatePlanetDetails: NSNotification.Name {
        return NSNotification.Name("updatePlanetDetails")
    }
}
