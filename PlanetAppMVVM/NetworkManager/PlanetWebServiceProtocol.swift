//
//  PlanetWebServiceProtocol.swift
//  PlanetAppMVVM
//
//  Created by Pooja Awati on 21/08/2019.
//  Copyright © 2019 Pooja Awati. All rights reserved.
//

import Foundation

protocol PlanetWebServiceProtocol {
    func fetchPlanetData(completion: @escaping (_ planet : Planet?, _ error: Error?) -> Void)
}
