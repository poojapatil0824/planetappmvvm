//
//  PlanetWebService.swift
//  PlanetAppMVVM
//
//  Created by Pooja Awati on 18/08/2019.
//  Copyright © 2019 Pooja Awati. All rights reserved.
//


import Foundation

class PlanetWebService {
}

extension PlanetWebService: PlanetWebServiceProtocol {
    func fetchPlanetData(completion: @escaping (_ planet : Planet?, _ error: Error?) -> Void) {
        let url = URL(string: "https://swapi.co/api/planets/?page=1")

        guard let downloadURL = url else {return}
        URLSession.shared.dataTask(with: downloadURL) { data, urlResponse, error in
            guard let data = data, error == nil, urlResponse != nil else {
                print("\(error.debugDescription)")
                return
            }
            do {
                let decoder = JSONDecoder()
                let planetList = try decoder.decode(Planet.self, from: data)
                return completion(planetList, nil)

            }catch {
                print("Something went wrong", error)
            }
            }.resume()
    }
}
